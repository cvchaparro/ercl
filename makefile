.PHONY: build run clean

GPP = g++

APP = ercl

SRC = $(shell ls *.cpp)
DEP = $(shell ls *.hpp)
OBJ = $(SRC:cpp=o)

%.o: %.cpp $(DEP)
	$(GPP) -Wall -std=c++11 -c $< -g

build: $(OBJ)
	$(GPP) -o $(APP) $(OBJ)

run: build
	./$(APP)

clean:
	rm -rf *~
	rm -rf *.o
	rm -rf $(APP)
