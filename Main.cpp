/**
 * file: Main.cpp
 *
 * brief: This file contains the main driver for the program which interacts
 * with the user.
 *
 * author: Cameron Vincent Chaparro
 * date: 30 April 2014
 */

// STL headers
#include <stdexcept> // invalid_argument, out_of_range
#include <iostream>  // cout, cin, endl, getline, etc
#include <cstdlib>   // rand, srand
#include <iomanip>   // setw
#include <string>    // string, to_string
#include <ctime>     // time

// Custom headers
#include "Constants.hpp"
#include "Bits.hpp"

// Function declarations
uint16_t get_integer_input(std::string& prompt, uint16_t min, uint16_t max);
uint16_t display_menu(std::string& title, std::string* opts, uint16_t num_opts);
void display_menu_top();
void display_menu_title(std::string& title);
void display_menu_separator();
void display_menu_bottom();

////////////////////////////////////////////////////////////////////////////////

/**
 * main
 *
 * brief: The main function is basically the work horse of the program. This
 * function is responsible for all the interaction with the user and the main
 * program loop.
 *
 * parameters:
 *  argc - The number of command line arguments.
 *  argv - The list of command line arguments.
 *
 * returns:
 *  0 - Returned upon successful completion of the program.
 */
int
main(int argc, char** argv)
{
  // Turn explanations of the algorithm on or off.
  bool explanation = false;

  // Bits object that deals with SEC and DED functionality.
  Bits* b = NULL;

  // Stores the current menu's title.
  std::string title;

  // Stores the current menu's options.
  std::string opts[6];

  // Stores the user's pick of the menu options.
  uint16_t menu_opt = 0;

  // Start the main program loop for processing user requests.
  while (true) {
    // Determines if bit values were initialized or not.
    bool initialized = false;

    // Determines if a bit (or two bits) has (or have) already been corrupted.
    // NOTE: This is only used in order to prevent the user from selecting 'Bit
    // Configuration Menu' option 3 (corrupt 1 or 2 bits) multiple times before
    // having the erroneous bit corrected, or re-initializing bits in the case
    // of 2 erroneous bits.
    bool corrupted = false;

    // Determines if a bit configuration is uncorrectable.
    // NOTE: This is only used to prevent the user from selecting 'Bit 
    // Configuration Menu' option 4 (correct 1 bit or try to correct 2 bits)
    // multiple times upon finding an uncorrectable error (i.e. 2 bits were
    // corrupted) before they re-initialize the bits.
    bool uncorrectable = false;

    // Determines if we should use random initial bit values, or if the user
    // prefers to initialize them.
    bool rand_bit_vals = true;

    // Stores the number of data bits to use in the simulation.
    uint16_t num_data_bits = 8;

    // Stores the number of bits to corrupt.
    uint16_t num_bits_corr = 1;

    // Set up the main menu information.
    title   = "MAIN MENU";
    opts[0] = "Start a new bit configuration.";
    opts[1] = "Toggle explanation of the algorithm.";
    opts[2] = "Quit the program.";

    // Display the menu and get the user's menu option.
    menu_opt = display_menu(title, opts, 3);

    // Act based on the user's menu option...
    if (menu_opt == 1) {
      /*************************************************************************
       * They chose to start a new bit configuration, so we should make sure
       * everything is set up for them.
       ************************************************************************/

      // If we already have a Bits object created, delete it and start fresh,
      // keeping the user's selection of whether or not explanations should be
      // displayed to them.
      if (b != NULL) {
        delete b;
        b = NULL;
      }
      b = new Bits(num_data_bits);
      b->set_explanation(explanation);

      // Display the 'Bit Configuration Menu' until the user decides to go back
      // to the main menu (i.e. they choose option 6)
      while (true) {
        // Set up the 'Bit Configuration Menu' information.
        title   = "BIT CONFIGURATION MENU";
        opts[0] = "Specify the number of bits to use. (default = 8)";
        opts[1] = "Specify bit values. (default = random values)";
        opts[2] = "Specify number of bits to corrupt. (default = 1)";
        opts[3] = "Correct the current bit configuration.";
        opts[4] = "Print the current configuration.";
        opts[5] = "Back to the 'main' menu.";

        // Display the menu and get the user's menu option.
        menu_opt = display_menu(title, opts, 6);

        if (menu_opt == 1) {
          /*********************************************************************
           * They want to specify a specific number of bits, so we will allow
           * them to change the number of bits until they're satisfied with the
           * selection.
           ********************************************************************/

          // Set up the 'Specify # of Bits Menu' information.
          title   = "SPECIFY # OF BITS MENU";
          opts[0] = "Use 8 bits. (default)";
          opts[1] = "Use 16 bits.";
          opts[2] = "Use 32 bits.";
          opts[3] = "Back to the 'bit configuration' menu.";

          // Display the 'Specify # of Bits Menu' until the user decides they
          // are satisfied with the number of data bits to be used and they
          // choose to go back to the 'Bit Configuration Menu' (i.e. they choose
          // option 4)
          while (true) {
            // Determines if the value of the data bits was changed.
            bool changed = false;

            // Display the menu and get the user's menu option.
            menu_opt = display_menu(title, opts, 4);

            // There is no reason to change the number of data bits if they
            // chose the number of bits we are already using.
            if (menu_opt == 1 && num_data_bits != 8) {
              /*****************************************************************
               * They want to change the number of data bits we should use to 8
               * data bits.
               ****************************************************************/

              num_data_bits = 8;
              changed = true;
            } else if (menu_opt == 2 && num_data_bits != 16) {
              /*****************************************************************
               * They want to change the number of data bits we should use to 16
               * data bits.
               ****************************************************************/

              num_data_bits = 16;
              changed = true;
            } else if (menu_opt == 3 && num_data_bits != 32) {
              /*****************************************************************
               * They want to change the number of data bits we should use to 32
               * data bits.
               ****************************************************************/

              num_data_bits = 32;
              changed = true;
            } else if (menu_opt == 4) {
              /*****************************************************************
               * They want to go back to the 'Bit Configuration Menu'.
               ****************************************************************/

              opts[3][0] += 32;
              std::cout << std::endl << "Going " << opts[3] << std::endl;
              opts[3][0] -= 32;
              break;
            }

            if (changed) {
              /*****************************************************************
               * The number of data bits was changed, so go ahead and update our
               * Bits object to reflect their change.
               ****************************************************************/

              std::cout << std::endl << "Setting number of data bits to ";
              std::cout << num_data_bits;
              std::cout << "...";
              b->set_num_data_bits(num_data_bits);
              std::cout << "[done]" << std::endl;

              // Show that nothing is initialized.
              initialized = false;

              // Show that the user is allowed to corrupt a bit (or two) again.
              corrupted = false;
              uncorrectable = false;
            } else {
              /*****************************************************************
               * Let the user know nothing will be changed since we are already
               * using the specified number of data bits in our current
               * configuration.
               ****************************************************************/

              std::cout << std::endl << "Since we are already using ";
              std::cout << num_data_bits << " data bits, nothing will be ";
              std::cout << "changed." << std::endl;
            }
          }
        } else if (menu_opt == 2) {
          /*********************************************************************
           * They want to specify how we should initialize the data bits, so we
           * should initialize each data bit accordingly, after we know and have
           * a value for it.
           ********************************************************************/

          // Determines if the current bit configuration was changed.
          bool changed = false;

          // Set up the 'Specify Initial Bit Values Menu' information.
          title   = "SPECIFY INITIAL BIT VALUES MENU";
          opts[0] = "Use random initial values for the bits. (default)";
          opts[1] = "Specify initial values for the bits.";
          opts[2] = "Back to the 'bit configuration' menu.";

          // Display the 'Specify Initial Bit Values Menu' until the user is
          // satisfied with the initial values for the bits.
          while (true) {
            // Display the menu and get the user's menu option.
            menu_opt = display_menu(title, opts, 3);

            // There is no reason to change whether we are using random bit
            // values if the user did not decide to do something different.
            if (menu_opt == 1 && !rand_bit_vals) {
              /*****************************************************************
               * They want to (re)initialize the current bit configuration with
               * randomly generated vaues, so update our configuration
               * accordingly.
               ****************************************************************/

              rand_bit_vals = true;
              changed = true;

              // If we are using random bit values, now is when we should seed
              // the random number generator.
              std::srand(std::time(NULL));
            } else if (menu_opt == 2 && rand_bit_vals) {
              /*****************************************************************
               * They want to specify initial values for the current bit
               * configuration, so update our configuration accordingly.
               ****************************************************************/

              rand_bit_vals = false;
              changed = true;
            } else if (menu_opt == 3) {
              /*****************************************************************
               * They want to go back to the 'Bit Configuration Menu', so
               * confirm their choice and go back.
               ****************************************************************/

              opts[2][0] += 32;
              std::cout << std::endl << "Going " << opts[2] << std::endl;
              opts[2][0] -= 32;
              break;
            }

            // If the bit values have already been initialized randomly and, the
            // user hasn't changed their mind and specified them, have the text
            // that gets displayed be more accurate (i.e. it will say that we're
            // re-initializing bit values, rather than just initializing them).
            if (initialized && rand_bit_vals && !changed) {
              changed = true;
            }

            // Holds the total number of bits (including data bits, check bits,
            // and the parity bit) in the current bit configuration.
            uint16_t num_bits = b->get_num_bits();

            // Let the user know we are randomly initializing data, if that is
            // what we are doing.
            if (rand_bit_vals) {
              std::cout << std::endl;
              if (changed) { std::cout << "Re-"; }
              std::cout << "Initializing bit values randomly...";
            } else {
              std::cout << std::endl;
            }

            // Stores the value entered by the user.
            uint16_t val = 0;

            // Loop through the bits in the configuration setting each one to
            // the user's desired value, or to a randomly generated value, if
            // they chose to have them randomly generated.
            for (uint16_t i = 3, j = 1; i < num_bits; i++) {
              // We aren't setting check bits, so skip over them.
              if (b->is_power_of_two(i)) { continue; }

              // Depending on how the user wants bit values initialized,
              // initialize the current data bit.
              if (!rand_bit_vals) {
                // Set up the prompt string
                std::string prompt;
                prompt  = "Enter the value for bit ";
                prompt += std::to_string(j++) + ":   ";

                // Get the input from the user. The reason for which we allow
                // any value between 0 and 0xffff is because we are only
                // taking the parity of the value, so if the user does not want
                // to enter solely 0s and 1s, they do not have to.
                while (true) {
                  val = get_integer_input(prompt, 0, 0xaaaa);
                  if (val == 0xaaab) {
                    std::cin.clear();
                    std::cout << "Valid values are 0 - " << 0xaaaa << std::endl;
                  } else {
                    break;
                  }
                }
              } else {
                // In order to get somewhat of a variety of values, get a
                // random value between 0 and 99, then we will just use the
                // parity of that value.
                val = std::rand() % 100;
              }

              // Set the bit's new value (once again, based on the parity of
              // the given value).
              b->set_bit_at(i, val % 2);
            }

            // Set the check bits and the parity bit of the new piece of data.
            b->set_check_bits();
            b->set_parity_bit();

            // Show that the data bits are now initialized.
            initialized = true;

            if (rand_bit_vals) {
              std::cout << "[done]" << std::endl;
            }

            // Show that the user can select option 3 (corrupt 1 or 2 bits) now.
            corrupted = false;

            // Reset the changed variable.
            changed = false;

            // Reset the uncorrectable variable.
            uncorrectable = false;
          }
        } else if (menu_opt == 3) {
          /*********************************************************************
           * They want to corrupt a bit (or possibly two bits) so we need to
           * allow them to do so in such a way that they don't corrupt the same
           * bit twice, and we must make sure they corrupt a bit (or two) of
           * data that is already initialized.
           ********************************************************************/

          // If the option to corrupt a bit was already chosen, without first
          // having the bits corrected or re-initialized, don't let the user
          // continue corrupting a bit (or bits) again.
          if (corrupted) {
            std::cout << std::endl;
            std::cout << ((num_bits_corr == 1) ? "A bit was" : "Two bits were");
            std::cout << " already corrupted, please ";
            std::cout << \
              ((num_bits_corr == 1) ? "have it corrected" : "reset the bits");
            std::cout << " before corrupting any more bits." << std::endl;

            continue;
         }

          // Determines if the current bit configuration was changed.
          bool changed = false;

          // If the data isn't already initialized, let's go ahead and perform
          // our default action and initialize it randomly. This is basically
          // the same functionality as if the user had chosen menu option 2 in
          // the 'Bit Configuration Menu' and then menu option 1 in the
          // subsequent menu.
          if (!initialized) {
            // Let the user know what we are doing.
            std::cout << std::endl << "Initializing bit values randomly...";

            // Seed the random number generator.
            std::srand(std::time(NULL));

            // Stores the total number of bits in the current configuration
            // (i.e. all the data bits, all the check bits, and the parity bit).
            uint16_t num_bits = b->get_num_bits();

            // Loop through the bits and initialize their values, randomly.
            for (uint16_t i = 3; i < num_bits; i++) {
              // Don't mess with the check bits right now.
              if (b->is_power_of_two(i)) { continue; }

              // Get a random value between 0 and 99 and use it's parity to set
              // the current data bit's value.
              uint16_t val = std::rand() % 100;
              b->set_bit_at(i, val % 2);
            }

            // Set the check bits and the parity bit for the current bit
            // configuration.
            b->set_check_bits();
            b->set_parity_bit();

            // Show that we have an initialized bit configuration, now.
            initialized = true;

            std::cout << "[done]" << std::endl;
          }

          // Display the current bit configuration so it's easy to see what
          // change is going to be made after corrupting the one (or two) bits.
          std::cout << std::endl << *b << std::endl;

          // Set up the 'Specify # of Bits to Corrupt Menu' information.
          title   = "SPECIFY # OF BITS TO CORRUPT MENU";
          opts[0] = "Corrupt 1 bit - to demonstrate SEC. (default)";
          opts[1] = "Corrupt 2 bits - to demonstrate DED.";
          opts[2] = "Back to the 'bit configuration' menu.";

          // Display the 'Specify # of Bits to Corrupt Menu' until the user is
          // satisfied with the number of bits they want to corrupt.
          while (true) {
            // Display the menu and get the user's menu option.
            menu_opt = display_menu(title, opts, 3);

            // Stores the total number of bits in the current bit configuration.
            uint16_t num_bits = b->get_num_bits();

            // Stores the value of the positions at which the user wants the
            // bits to be corrupted. We initialize them to an invalid value, for
            // input validation purposes.
            uint16_t corrupt[2] = { 0, 0 };
            corrupt[0] = corrupt[1] = num_bits + 1;

            // There is no reason to change anything if the user selected the
            // option that corresponds to what we already are going to do.
            if (menu_opt == 1 && num_bits_corr != 1) {
              /*****************************************************************
               * They want to corrupt only one bit.
               ****************************************************************/

              num_bits_corr = 1;
              changed = true;
            } else if (menu_opt == 2 && num_bits_corr != 2) {
              /*****************************************************************
               * They want to corrupt two bits.
               ****************************************************************/

              num_bits_corr = 2;
              changed = true;
            } else if (menu_opt == 3) {
              opts[2][0] += 32;
              std::cout << std::endl << "Going " << opts[2] << std::endl;
              opts[2][0] -= 32;
              break;
            }

            if (changed) {
              std::cout << std::endl << "The number of bits to corrupt was set";
              std::cout << " to " << num_bits_corr << "." << std::endl;
            } else {
              std::cout << std::endl << "Since we are already having ";
              std::cout << num_bits_corr << " bits get corrupted, the number ";
              std::cout << "of bits to corrupt will not be changed.";
              std::cout << std::endl;
            }

            // Set up the menu title.
            title = "SPECIFY BIT TO CORRUPT MENU";

            // Stores the menu options of the bits that can be corrupted.
            std::string* bit_opts = new std::string[num_bits + 1];

            // Build up the options string array that should be displayed on
            // the 'Specify Bit to Corrupt Menu'.
            for (uint16_t i = 0, j = 1; i < num_bits; i++) {
              bit_opts[i] = "Corrupt ";
              if (i == 0) {
                bit_opts[i] += "parity bit (P)";
              } else if (b->is_power_of_two(i)) {
                std::string i_str = std::to_string(i);
                bit_opts[i] += "check bit " + i_str + " (C" + i_str + ")";
              } else {
                std::string j_str = std::to_string(j++);
                bit_opts[i] += "data bit " + j_str + " (M" + j_str + ")";
              }
            }
            bit_opts[num_bits]  = "Back to the 'specify # of bits to ";
            bit_opts[num_bits] += "corrupt' menu.";

            // Depending on how many bits the user chooses to corrupt, we'll
            // display the 'Specify Bit to Corrupt Menu' that many times
            // asking them for the bit they want to corrupt.
            for (uint16_t i = 0; i < num_bits_corr; i++) {
              // Display the menu and get the user's menu option.
              menu_opt = display_menu(title, bit_opts, num_bits + 1);

              if (menu_opt == num_bits + 1) {
                // If the user chooses the option num_bits, it means they choose
                // to go back to the previous menu, so don't corrupt anything.
                num_bits_corr = 0;
                std::cout << "No bits will be corrupted.";
                bit_opts[num_bits][0] += 32;
                std::cout << std::endl << "Going " << bit_opts[num_bits];
                std::cout << std::endl;
                bit_opts[num_bits][0] -= 32;
                break;
              } else if ((i == 1) && (corrupt[0] == menu_opt - 1)) {
                // If this is the second bit the user is corrupting, make sure
                // they didn't specify the same bit as the first bit they
                // chose to corrupt.
                std::cout << std::endl << "Please choose another bit.";
                std::cout << std::endl;
                i--;
              } else if (corrupt[i] == num_bits + 1) {
                // Otherwise, allow them to corrupt the bit at the specified
                // position in the augmented bit array.
                corrupt[i] = menu_opt - 1;
              }
            }

            std::cout << std::endl;

            // Once the user decides what bit (or bits) to corrupt, actually
            // go about corrupting them.
            for (uint16_t i = 0; i < num_bits_corr; i++) {
              std::cout << "Corrupting bit at position " << corrupt[i] << "...";
              b->flip(corrupt[i]);
              std::cout << "[done]" << std::endl;

              // Show that the user cannot select option 3 now.
              if (i == num_bits_corr - 1) { corrupted = true; }
            }

            // Free the memory we used for the menu options.
            delete [] bit_opts;
            bit_opts = NULL;

            // Get out of this loop.
            break;
          }
        } else if (menu_opt == 4) {
          /*********************************************************************
           * They want to correct the bit configuration (if it was corrupted),
           * so perform any needed correction and display the current
           * configuration to the user.
           ********************************************************************/

          if (uncorrectable) {
            std::cout << std::endl << "Two bits were corrupted, please have ";
            std::cout << "the bits reset before trying to correct anything. (A";
            std::cout << " two bit error is the result of hardware failure and";
            std::cout << " cannot be corrected.)" << std::endl;
            continue;
          }

          std::cout << std::endl << "Current bit configuration" << std::endl;
          std::cout << std::endl << *b << std::endl;
          uint16_t old_parity = b->get_bit_at(0);
          uint16_t syndrome = b->calculate_syndrome();
          if (syndrome == 0) {
            std::cout << std::endl << "No data bits were corrupted!";
            std::cout << std::endl;

            // Figure out if the parity bit was the one that was corrupted or,
            // if indeed, no bits were corrupted at all.
            uint16_t new_parity = b->calculate_parity_bit();
            if (new_parity != old_parity) {
              std::cout << std::endl << "The parity bit was corrupted. We need";
              std::cout << " to recalculate the parity bit." << std::endl;
              std::cout << "The old parity bit was set to " << old_parity << " ";
              std::cout << "the new parity bit was set to " << new_parity;
              std::cout << std::endl;

              // Calculate (and set) the parity bit so that it is corrected.
              b->set_parity_bit();

              // Update 'old_parity' to reflect the correct parity bit. This
              // prevents detecting hardware failure if only the parity bit gets
              // corrupted.
              old_parity = new_parity;
            }
          } else {
            // Tell the user where the error occurred, and correct it.
            std::cout << std::endl << "The error occured at bit position:   ";
            std::cout << syndrome << std::endl;

            if (b->is_power_of_two(syndrome)) {
              std::cout << "But no data bits were corrupted (it was check bit ";
              std::cout << syndrome << ") so we just have to recalculate the ";
              std::cout << "check bits." << std::endl;

              // If a check bit was corrupted, recalculate all of the check bits
              // to make sure everything still squares away as it should.
              b->set_check_bits();
            } else {
              // If a data bit was corrupted, flip it.
              b->flip(syndrome);
            }
          }

          // Calculate the parity bit and compare it to the original parity bit.
          // If they differ, 2 bits were corrupted and that means that there was
          // hardware failure. Another cause for failure is when the syndrome is
          // larger than the number of bits in the augmented array.
          uint16_t new_parity = b->calculate_parity_bit();
          if (old_parity != new_parity || syndrome >= b->get_num_bits()) {
            std::cout << std::endl << "Hardware failure was detected because ";
            std::cout << "actually 2 bits were corrupted." << std::endl;

            if (old_parity != new_parity) {
              std::cout << "In other words, the bit at position " << syndrome;
              std::cout << " getting flipped is actually the result of a ";
              std::cout << "compounded error caused by 2 bits getting ";
              std::cout << "corrupted." << std::endl << "The parity bit was ";
              std::cout << old_parity << " but it was supposed to be ";
              std::cout << new_parity << "." << std::endl;
            } else {
              std::cout << "We know there was an error since the calculated ";
              std::cout << "syndrome is out of the bounds of the augmented ";
              std::cout << std::endl << "bit array, which shows us that we ";
              std::cout << "can't correct the bit error even if we wanted to ";
              std::cout << "because we don't have a way to figure out where ";
              std::cout << "the bit error actually occurred." << std::endl;
            }

            // Since we now know that the error is uncorrectable, set the flag
            // indicating that the user should not try to have bits corrected
            // again.
            uncorrectable = true;
          } else {
            std::cout << std::endl << "Corrected bit configuration";
            std::cout << std::endl << std::endl << *b << std::endl;

            // Set the corrupted flag to false to allow the user to corrupt a
            // bit (or two bits) again.
            corrupted = false;
          }
        } else if (menu_opt == 5) {
          /*********************************************************************
           * They want to see the current bit configuration, so display it.
           ********************************************************************/
          std::cout << std::endl << "The bit configuration is:" << std::endl;
          std::cout << std::endl << *b << std::endl;
        } else if (menu_opt == 6) {
          /*********************************************************************
           * They want to go back to the main menu.
           ********************************************************************/
          opts[5][0] += 32;
          std::cout << std::endl << "Going " << opts[5] << std::endl;
          opts[5][0] -= 32;
          break;
        }
      }
    } else if (menu_opt == 2) {
      /*************************************************************************
       * They chose to turn on (or off) the algorithm explanation, so set up the
       * Bits object to display (or undisplay) semi-verbose explanations of how
       * check bits, parity bit, and syndrome are calculated.
       ************************************************************************/

      // Determines if the status of the explanation flag has been changed.
      bool changed = false;

      // Set up the 'Toggle Explanation Menu' information.
      title   = "TOGGLE EXPLANATION MENU";
      opts[0] = "Turn explanation of algorithm on.";
      opts[1] = "Turn explanation of algorithm off. (default)";
      opts[2] = "Back to the 'main' menu.";

      // Display the 'Toggle Explanation Menu' until the user is satisfied with
      // whether or not they want to see extra information about the algorithm
      // and what, and how, calculations are performed.
      while (true) {
        // Display the menu and get the user's menu option.
        menu_opt = display_menu(title, opts, 3);

        // As long as the user didn't choose to exit, update the explanation
        // indicator, as necessary.
        if (menu_opt != 3) {
          // There is no need to change anything if the user's choice coincides
          // with the currently selected value.
          if (menu_opt == 1 && !explanation) {
            explanation = true;
            changed = true;
          } else if (menu_opt == 2 && explanation) {
            explanation = false;
            changed = true;
          }
        } else if (menu_opt == 3) {
          opts[2][0] += 32;
          std::cout << std::endl << "Going " << opts[2] << std::endl;
          opts[2][0] -= 32;
          break;
        }

        if (changed) {
          std::cout << std::endl << "Turned explanation ";
          std::cout << ((explanation) ? "on" : "off") << "." << std::endl;
        } else {
          std::cout << std::endl << "Since explanations were already turned ";
          std::cout << ((explanation) ? "on " : "off ") << "nothing will be ";
          std::cout << "changed." << std::endl;
        }

        // Reset the changed flag.
        changed = false;
      } 
    } else if (menu_opt == 3) {
      /*************************************************************************
       * They want to break out of the program, so let's do it.
       ************************************************************************/
      std::cout << std::endl << "Quitting..." << std::endl;
      break;
    }
  }

  // Make sure we clean up (return) all our used memory, if we still need to.
  if (b != NULL) {
    delete b;
    b = NULL;
  }

  return 0;
} // main

////////////////////////////////////////////////////////////////////////////////

/**
 * get_integer_input
 *
 * brief: Prompts the user for integer input, until a valid value is entered.
 *
 * parameters:
 *  prompt - The text to display to the user prompting them for input.
 *  min - The minimum valid value.
 *  max - The maximum valid value.
 *
 * returns:
 *  Nothing
 */
uint16_t
get_integer_input(std::string& prompt, uint16_t min, uint16_t max)
{
  std::string input;
  uint16_t value = 0;
  while (true) {
    std::cout << prompt;
    std::getline(std::cin, input);

    try {
      value = std::stoi(input);
      if (value >= min && value <= max) {
        return value;
      }
    } catch (std::invalid_argument& ia) {
      return max + 1;
    } catch (std::out_of_range& oor) {}

    // If we got here, ther was an error.
    std::cout << "Valid values are " << min << " - " << max << std::endl;
  }
} // get_integer_input

/*******************************************************************************
 * The following is a brief description of each of the unicode characters used
 * below in our displaying of each part of the menus:
 * (NOTE: The "(i.e. ...)" part is a basic representation of what the character
 * being described looks like.)
 * *****************************************************************************
 *  2500 - horizontal bar       (i.e. --)
 *
 *  2502 - vertical bar         (i.e. |)
 *                                     _
 *  250C - top left corner      (i.e. | )
 *                                    _   
 *  2510 - top right corner     (i.e.  |)
 *
 *  2514 - bottom left corner   (i.e. |_)
 *
 *  2518 - bottom right corner  (i.e. _|)
 *
 *  251C - left-facing tee      (i.e. |-)
 *
 *  2524 - right-facing tee     (i.e. -|)
 ******************************************************************************/

/**
 * display_menu
 *
 * brief: Displays a menu with the specified title and options and then prompts
 * the user to choose a menu option until their selection is valid (i.e. in the
 * range of the possible menu options - usually 1 to num_opts.
 *
 * parameters:
 *  title - The menu's title
 *  opts - An array of size 'num_opts' containing the menu's possible options.
 *  num_opts - The number of options allowable for menu selection and the size
 *   of the 'opts' array.
 *
 * returns:
 *  value - The user's valid menu option is returned.
 */
uint16_t
display_menu(std::string& title, std::string* opts, uint16_t num_opts)
{
  std::cout << std::endl;
  display_menu_top();
  display_menu_title(title);
  display_menu_separator();

  for (uint16_t i = 0; i < num_opts; i++) {
    if (i == num_opts - 1) {
      std::cout << "\u2502" << std::setw(BORDER_LENGTH + 4) << "\u2502";
      std::cout << std::endl;
    }
    std::cout << "\u2502 " << (i + 1) << ". " << opts[i];
    if (i < 9) {
      std::cout << std::setw(BORDER_LENGTH - opts[i].length()) << "\u2502";
      std::cout << std::endl;
    } else {
      std::cout << std::setw(BORDER_LENGTH - opts[i].length() - 1) << "\u2502";
      std::cout << std::endl;
    }
  }

  display_menu_bottom();

  uint16_t value = 0;

  while (true) {
    std::string prompt = "Pick an option (1 - MAX):   ";
    prompt.replace(prompt.find("MAX"), 3, std::to_string(num_opts));
    value = get_integer_input(prompt, 1, num_opts);

    if (value == num_opts + 1) {
      std::cin.clear();
      std::cout << "Valid values are 1 - " << num_opts << std::endl;
    } else {
      break;
    }
  }

  return value;
} // display_menu

/**
 * display_menu_top
 *
 * brief: Displays the top border, with the specified length, of a menu using
 * unicode box drawing characters.
 *
 * parameters:
 *  None
 *
 * returns:
 *  Nothing
 */
void
display_menu_top()
{
  std::cout << "\u250C";
  for (uint16_t i = 0; i <= BORDER_LENGTH; i++) {
    std::cout << "\u2500";
  }
  std::cout << "\u2510" << std::endl;
} // display_menu_border

/**
 * display_menu_title
 *
 * brief: Displays the title of the menu, with the specified length, centered,
 * using unicode box drawing characters.
 *
 * parameters:
 *  title - The string that should be displayed as the menu title.
 *
 * returns:
 *  Nothing
 */
void
display_menu_title(std::string& title)
{
  std::cout << "\u2502";
  for (uint16_t i = 0; i <= BORDER_LENGTH; i++) {
    if (i == (BORDER_LENGTH / 2) - (title.length() / 2)) {
      std::cout << title;
      i += title.length();
    }
    std::cout << " ";
  }
  std::cout << "\u2502" << std::endl;
} // display_menu_title

/**
 * display_menu_separator
 *
 * brief: Displays a separation bar in the middle of the menu.
 *
 * parameters:
 *  None
 *
 * returns:
 *  Nothing
 */
void
display_menu_separator()
{
  std::cout << "\u251C";
  for (uint16_t i = 0; i <= BORDER_LENGTH; i++) {
    std::cout << "\u2500";
  }
  std::cout << "\u2524" << std::endl;
} // display_menu_separator

/**
 * display_menu_bottom
 *
 * brief: Displays the bottom border of the menu with specified length.
 *
 * parameters:
 *  None
 *
 * returns:
 *  Nothing
 */
void
display_menu_bottom()
{
  std::cout << "\u2514";
  for (uint16_t i = 0; i <= BORDER_LENGTH; i++) {
    std::cout << "\u2500";
  }
  std::cout << "\u2518" << std::endl;
} // display_menu_border

