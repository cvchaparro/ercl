/**
 * file: Bits.cpp
 *
 * brief: This file contains the implementation of the Bits class.
 *
 * author: Cameron Vincent Chaparro
 * date: 25 April 2014
 */

// STL headers
#include <iostream>
#include <string>

// Custom headers
#include "Constants.hpp"
#include "Bits.hpp"

////////////////////////////////////////////////////////////////////////////////

/**
 * Bits (default constructor)
 *
 * brief: Creates a default-valued Bits object where, by default-valued, we mean
 * 8 data bits.
 *
 * parameters:
 *  None
 *
 * returns:
 *  Nothing
 */
Bits::Bits()
{
  // Set everything to default values.
  bits_ = NULL;
  set_num_data_bits(8);
  set_explanation(false);
} // Bits

/**
 * Bits (constructor)
 *
 * brief: Creates a Bits object with 'num_data_bits' number of data bits and the
 * appropriate number of check bits for SEC / DED. If an invalid number of data
 * bits are specified, the default value of 8 data bits will be used.
 *
 * parameters:
 *  num_data_bits - The number of data bits to use for the simulation.
 *
 * returns:
 *  Nothing
 */
Bits::Bits(const uint16_t num_data_bits)
{
  bits_ = NULL;
  set_num_data_bits(num_data_bits);
  set_explanation(false);
} // Bits

/**
 * Bits (constructor)
 *
 * brief: Creates a Bits object with 'num_data_bits' number of data bits and the
 * appropriate number of check bits for SEC / DED, and the bits will be
 * initialized to the values passed in the 'bits' array. If an invalid number of
 * data bits are specified, the default value of 8 data bits will be used.
 *
 * parameters:
 *  num_data_bits - The number of data bits to use for the simulation.
 *  bits - The array of desired initial bit values.
 *
 * returns:
 *  Nothing
 */
Bits::Bits(const uint16_t num_data_bits, const uint16_t* bits)
{
  bits_ = NULL;
  set_num_data_bits(num_data_bits);
  set_explanation(false);

  if (bits != NULL) {
    uint16_t length = get_num_bits();
    for (uint16_t i = 0, j = 0; i < length; i++) {
      (i == 0 || is_power_of_two(i)) ? bits_[i] = 0 : bits_[i] = bits[j++];
    }

    set_check_bits();
    set_parity_bit();
  }
} // Bits

////////////////////////////////////////////////////////////////////////////////

/**
 * ~Bits (destructor)
 *
 * brief: Performs memory clean up at the end of a Bits object's life.
 *
 * parameters:
 *  None
 *
 * returns:
 *  Nothing
 */
Bits::~Bits()
{
  if (bits_ != NULL) {
    delete [] bits_;
    bits_ = NULL;
  }
  num_data_bits_  = 0;
  num_check_bits_ = 0;
  explanation_    = false;
} // ~Bits

////////////////////////////////////////////////////////////////////////////////

/**
 * get_bit_at
 *
 * brief: Returns the bit value at the specified position number. If the value
 * of 'pos' is out of range, 2 is returned to signify an error in the index
 * value.
 *
 * parameters:
 *  pos - The position of the bit whose value should be returned.
 *
 * returns:
 *  bits_[pos] - Returned if the value of 'pos' is in the range of valid indices
 *   in the bits array.
 *  2 - Returned if 'pos' is out of bounds.
 */
uint16_t
Bits::get_bit_at(const uint16_t pos) const
{
  return (pos < get_num_bits()) ? bits_[pos] : 2;
} // get_bit_at

/**
 * get_num_check_bits
 *
 * brief: Returns the number of check bits being used in the simulation. The
 * value returned does include a parity bit.
 *
 * parameters:
 *  None
 *
 * returns:
 *  num_check_bits_ - Always will be returned.
 */
uint16_t
Bits::get_num_check_bits() const
{
  return num_check_bits_;
} // get_num_check_bits

/**
 * get_num_data_bits
 *
 * brief: Returns the number of data bits being used in the simulation.
 *
 * parameters:
 *  None
 *
 * returns:
 *  num_data_bits_ - Always will be returned.
 */
uint16_t
Bits::get_num_data_bits() const
{
  return num_data_bits_;
} // get_num_data_bits

/**
 * get_num_bits
 *
 * brief: Returns the total number of bits being used in the simulation. That
 * is, all the data bits and all the check bits. This function is equivalent to
 * get_num_check_bits() + get_num_data_bits()
 *
 * parameters:
 *  None
 *
 * returns:
 *  num_data_bits_ + num_check_bits_ - Always will be returned.
 */
uint16_t
Bits::get_num_bits() const
{
  return num_data_bits_ + num_check_bits_;
} // get_num_bits

/**
 * get_explanation
 *
 * brief: Returns whether or not explanations of calculations are displayed to
 * the user.
 *
 * parameters:
 *  None
 *
 * returns:
 *  explanation_ - Always will be returned.
 */
bool
Bits::get_explanation() const
{
  return explanation_;
} // get_explanation

////////////////////////////////////////////////////////////////////////////////

/**
 * flip
 *
 * brief: Flips the bit at the specified position and returns true on succes.
 *
 * parameters:
 *  pos - The position of the bit at which the value should be flipped.
 *
 * returns:
 *  true - Returned if the value at the specified position was flipped.
 *  false - Returned if nothing is changed.
 */
bool
Bits::flip(const uint16_t pos)
{
  if (pos < get_num_bits()) {
    return set_bit_at(pos, ~bits_[pos]);
  }
  return false;
} // flip

/**
 * set_bit_at
 *
 * brief: Sets the value of the bit at the specified position. If an invalid bit
 * value is specified (i.e. a value other than 0 or 1), the parity of 'val' will
 * be used to set the value of the desired bit, instead. If an invalid index is
 * specified, no bit value will be changed.
 *
 * parameters:
 *  pos - The position of the bit at which the value should be set.
 *  val - The bit value that should be stored at the specified position.
 *
 * returns:
 *  true - Returned if the value at the specified position was set to the
 *   desired value.
 *  false - Returned if nothing is changed.
 */
bool
Bits::set_bit_at(const uint16_t pos, const uint16_t val)
{
  if (pos < get_num_bits()) {
    bits_[pos] = (val % 2);
    return true;
  }
  return false;
} // set_bit_at

/**
 * set_num_data_bits
 *
 * brief: Sets the number of data bits to be used for the current simulation. If
 * an invalid number of data bits is specified, the default value of 8 data bits
 * will be used, unless a valid configuration is already in place - that is, if
 * set_num_data_bits has already been called previously.
 * Note, also, that this function updates the number of check bits and the bits
 * array accordingly, so as to keep all data consistent.
 *
 * parameters:
 *  num_data_bits - The number of data bits to use in the simulation.
 *
 * returns:
 *  true - Returned upon successfully setting the number of data bits to the new
 *   specified value.
 *  false - Returned if nothing is changed (i.e. the number of bits is the same
 *   as it was before the call), or the number of bits is an invalid value.
 */
bool
Bits::set_num_data_bits(const uint16_t num_data_bits)
{
  if (num_data_bits_ == num_data_bits) {
    return false;
  }

  if (num_data_bits == 8  || num_data_bits == 16 || num_data_bits == 32) {
    num_data_bits_  = num_data_bits;
    num_check_bits_ = 5;
    switch(num_data_bits_) {
    case 32:
      num_check_bits_++;
    case 16:
      num_check_bits_++;
    }
  } else if (bits_ == NULL) {
    num_data_bits_  = 8;
    num_check_bits_ = 5;
  } else {
    return false;
  }

  if (bits_ != NULL) {
    delete [] bits_;
    bits_ = NULL;
  }

  uint16_t num_bits = get_num_bits();
  bits_ = new uint16_t[num_bits];
  for (uint16_t i = 0; i < num_bits; i++) { bits_[i] = 0; }

  return true;
} // set_num_data_bits

/**
 * set_check_bits
 *
 * brief: Sets the check bits for the currently stored data bits after having
 * them calculated.
 *
 * parameters:
 *  None
 *
 * returns:
 *  true - Returned upon successful completion of calculating and setting the
 *   check bits at their corresponding positions in the augmented array.
 *  false - Returned if an error occurs during any part of calculating or
 *   storing the check bits.
 */
bool
Bits::set_check_bits()
{
  // If explanations are on, turn them off so that the user doesn't get the
  // possibility of duplicate text.
  bool tmp_exp = explanation_;
  set_explanation(false);

  uint16_t* check_bits = calculate_check_bits();

  if (check_bits == NULL) {
    return false;
  }

  uint16_t  num_bits = get_num_bits();

  for (uint16_t i = 1, j = 0; i < num_bits; i++) {
    if (is_power_of_two(i)) {
      bits_[i] = check_bits[j++];
    }

    // If we are past the last check bit, break out of the loop to save a little
    // bit of time.
    if (i >= (1 << (num_check_bits_ - 2))) { break; }
  }

  delete [] check_bits;
  check_bits = NULL;

  // Set explanations back to their original state.
  set_explanation(tmp_exp);

  return true;
} // set_check_bits

/**
 * set_parity_bit
 *
 * brief: Sets the parity bit for the currently stored augmented bit list, after
 * having it calculated.
 *
 * parameters:
 *  None
 *
 * returns:
 *  true - Returned upon successful completion of calculating and setting the
 *   parity bit at position 0 in the augmented array.
 *  false - Returned if an error occurs during any part of calculating or
 *   storing the parity bit. (This will never actually be returned.)
 */
bool
Bits::set_parity_bit()
{
  // If explanations are on, turn them off so that the user doesn't get the
  // possibility of duplicate text.
  bool tmp_exp = explanation_;
  set_explanation(false);

  bits_[0] = calculate_parity_bit();

  // Set explanations back to their original state.
  set_explanation(tmp_exp);

  return true;
} // set_parity_bit

/**
 * set_explanation
 *
 * brief: Sets the variable that determines if verbose explanations should be
 * given of the steps taken to calculate check bits, parity bit, and syndrome.
 *
 * parameters:
 *  explanation - The inicator that determines if explanations should be
 *   displayed to the user.
 *
 * returns:
 *  explanation_ - Always will be returned.
 */
bool
Bits::set_explanation(const bool explanation)
{
  explanation_ = explanation;
  return explanation_;
} // set_explanation

////////////////////////////////////////////////////////////////////////////////

/**
 * calculate_check_bits
 *
 * brief: Calculates the check bits for the currently stored data bits, and
 * returns a new array containing the check bits.
 * Note, this function allocates a new block of memory in which the check bit
 * values are stored, it is up to the caller to release that memory.
 *
 * parameters:
 *  None
 *
 * returns:
 *  array of check bits - Returned if the check bits calculation succeeds for
 *   every check bit.
 */
uint16_t*
Bits::calculate_check_bits() const
{
  uint16_t  bit_index  = 0;
  uint16_t  num_bits   = get_num_bits();
  uint16_t* check_bits = new uint16_t[num_check_bits_ - 1];

  for (uint16_t i = 0; i < num_check_bits_ - 1; i++) { check_bits[i] = 0; }

  // Calculate each of the check bits by XOR'ing the bit value of every bit with
  // a 1 in its i-th position when considered as a binary number.
  for (uint16_t i = 1; i < num_bits; i++) {
    if (is_power_of_two(i)) {
      if (explanation_) {
        std::cout << std::endl << "Calculating check bit " << i << " as the ";
        std::cout << "XOR sum of the bit values at positions:" << std::endl;
        std::cout << "   ";
      }

      for (uint16_t j = i + 1; j < num_bits; j++) {
        if (explanation_ && ((i & j) == i)) {
          std::cout << j << " ";
        }

        check_bits[bit_index] ^= ((i & j) == i) ? bits_[j] : 0;
      }

      if (explanation_) { std::cout << std::endl; }

      bit_index++;
    }

    // If we are past the last check bit, break out of the loop to save a little
    // bit of time.
    if (i >= (1 << (num_check_bits_ - 2))) { break; }
  }

  if (explanation_) { std::cout << std::endl; }

  return check_bits;
} // calculate_check_bits

/**
 * calculate_parity_bit
 *
 * brief: Calculates the parity bit for the currently stored data bits and check
 * bits, and returns the value.
 *
 * parameters:
 *  None
 *
 * returns:
 *  parity - The parity of all the bits.
 */
uint16_t
Bits::calculate_parity_bit() const
{
  uint16_t num_bits = get_num_bits();
  uint16_t parity   = 0;

  if (explanation_) {
    std::cout << std::endl << "Calculating parity bit as XOR sum of all the ";
    std::cout << "bits (not including the value of the current parity bit).";
    std::cout << std::endl;
  }

  // Calculate the parity bit based on each of the bits in the bits array (even
  // including the check bits).
  for (uint16_t i = 1; i < num_bits; i++) {
    parity ^= bits_[i];
  }

  return parity;
} // calculate_parity_bit

/**
 * calculate_syndrome
 *
 * brief: Calculates the XOR sum of each of the current check bits and the check
 * bits passed in, and returns the value.
 *
 * parameters:
 *  K: The array of check bits with which the current check bits should be XOR'd
 *   to compute and return the syndrome.
 *
 * returns:
 *  syndrome - The value computed by XOR'ing the current check bits with the
 *   ones that were passed in, upon a successful calculation.
 *  get_num_bits() + 1 - Returned if some error occurs during the calculation of
 *   the syndrome value.
 */
uint16_t
Bits::calculate_syndrome() const
{
  uint16_t num_bits = get_num_bits();

  // Get the currently stored check bits
  uint16_t* K = new uint16_t[num_check_bits_ - 1];

  for (uint16_t i = 1, j = 0; i < num_bits; i++) {
    if (is_power_of_two(i)) {
      K[j++] = bits_[i];
    }

    // If we are past the last check bit, break out of the loop to save a little
    // bit of time.
    if (i >= (1 << (num_check_bits_ - 2))) { break; }
  }

  uint16_t position = 1;

  // Calculate the current check bits
  uint16_t* K_ = calculate_check_bits();

  std::cout << "      C";
  for (uint16_t i = num_check_bits_ - 1; i > 0; i--) {
    std::cout << std::to_string(position << (i - 1));
    if (i < 10) { std::cout << " "; }
    if (i > 1) { std::cout << " C"; }
  }
  std::cout << std::endl << "K  =   ";
  for (uint16_t i = num_check_bits_ - 1; i > 0; i--) {
    if (num_bits > 13 && i >= 5) { std::cout << " "; }
    std::cout << std::to_string(K[i - 1]) << "   ";
  }
  std::cout << std::endl << "K' =   ";
  for (uint16_t i = num_check_bits_ - 1; i > 0; i--) {
    if (num_bits > 13 && i >= 5) { std::cout << " "; }
    std::cout << std::to_string(K_[i - 1]) << "   ";
  }
  std::cout << std::endl;

  if (explanation_) {
    std::cout << std::endl << "The syndrome is calculated by performing an XOR";
    std::cout << " sum of the values in corresponding positions of K and K'. ";
    std::cout << std::endl << "That is, the value in position C1 of K is XOR'd";
    std::cout << " with the value in position C1 of K', etc." << std::endl;
    std::cout << "Then the syndrome is the position at which the bit was ";
    std::cout << "corrupted." << std::endl;
  }

  uint16_t syndrome = 0;

  for (uint16_t i = 0; i < num_check_bits_ - 1; i++) {
    // The steps here are as follows:
    //  1. Calculate K ^ K_
    //  2. Left-shift the value we just calculated over by i - 1 to make the
    //     calculation in Step 4 meaningful.
    //  3. Left-shift position over by i - 1 to make the calculation in Step 4
    //     meaningful.
    //  4. Perform a bitwise AND of the values calculated in Steps 2 and 3 to
    //     figure out if the newly calculated check bits at the specified
    //     position were different from the ones we received from the user.
    //  5. Add the value calculated in Step 4 to the current value of syndrome
    //     to get the position at which the error occured.
    syndrome += (((K[i] ^ K_[i]) << i) & (position << i));
  }

  delete [] K_;
  K_ = NULL;

  return syndrome;
} // calculate_syndrome

////////////////////////////////////////////////////////////////////////////////

/**
 * operator[]
 *
 * brief: Permits element access to a specified bit in the bit list.
 *
 * parameters:
 *  pos - The position in the bits list that will be returned.
 *
 * returns:
 *  bits_[pos] - Always returned assuming 'pos' is a valid position into the
 *   bits list. Otherwise, the return value is undefined.
 */
uint16_t&
Bits::operator[](const uint16_t pos)
{
  return bits_[pos];
} // operator[]

////////////////////////////////////////////////////////////////////////////////

/**
 * is_power_of_two
 *
 * brief: Returns true if the specified value is a power of two, and false
 * otherwise.
 *
 * parameters:
 *  val - The value which should be check to see if it is a power of two.
 *
 * returns:
 *  true - Returned if 'val' is a power of two.
 *  false - Returned if 'val' is not a power of two.
 */
bool
Bits::is_power_of_two(const uint16_t val) const
{
  uint32_t power = 1;
  for (uint16_t i = 0; i <= HIGHEST_POWER_OF_TWO; i++) {
    if (val == power) {
      return true;
    }

    power <<= 1;
  }
  return false;
} // is_power_of_two

////////////////////////////////////////////////////////////////////////////////

/**
 * operator<<
 *
 * brief: Overloads the output operator for the Bits class.
 *
 * parameters:
 *  os - The output stream to which the Bits class data should be written.
 *  b - The Bits object whose data should be written out to the output stream.
 *
 * returns:
 *  os - The updated output stream containing the Bits object's data.
 */
std::ostream&
operator<<(std::ostream& os, Bits& b)
{
  uint16_t num_bits   = b.get_num_bits();
  std::string content = " ";

  for (uint16_t i = num_bits - 1; i > 0; i--) {
    if (i < 10) { content += " "; }
    content += (std::to_string(i) + "  ");
  }
  content += " 0\n";

  for (uint16_t i = num_bits - 1, j = b.get_num_data_bits(); i > 0; i--) {
    if (b.is_power_of_two(i)) {
      if (i < 10) { content += " "; }
      content += ("C" + std::to_string(i));
    } else {
      if (j < 10) { content += " "; }
      content += ("M" + std::to_string(j));
      j--;
    }
    content += " ";
  }
  content += "  P\n\n  ";

  for (uint16_t i = num_bits - 1; i > 0; i--) {
    content += (std::to_string(b[i]) + "   ");
  }
  content += (std::to_string(b[0]) + "\n");

  return os << content;
} // operator<<
