/**
 * file: Bits.hpp
 *
 * brief: This file contains the declaration of the Bits class.
 *
 * author: Cameron Vincent Chaparro
 * date: 25 April 2014
 */

#ifndef _BITS_HPP_
#define _BITS_HPP_

// STL headers
#include <ostream>

// Custom headers
#include "Types.hpp"

class Bits {
public:
  // Constructors
  Bits();
  Bits(const uint16_t num_data_bits);
  Bits(const uint16_t num_data_bits, const uint16_t* bits);

  // Destructor
  ~Bits();

  // Accessors
  bool     get_explanation() const;
  uint16_t get_bit_at(const uint16_t pos) const;
  uint16_t get_num_check_bits() const;
  uint16_t get_num_data_bits() const;
  uint16_t get_num_bits() const;

  // Mutators
  bool flip(const uint16_t pos);
  bool set_bit_at(const uint16_t pos, const uint16_t val);
  bool set_num_data_bits(const uint16_t num_data_bits);
  bool set_explanation(const bool explanation);
  bool set_check_bits();
  bool set_parity_bit();

  // Bit calculations
  uint16_t* calculate_check_bits() const;
  uint16_t  calculate_parity_bit() const;
  uint16_t  calculate_syndrome() const;

  // Operators
  uint16_t& operator[](const uint16_t pos);

  // Other
  bool is_power_of_two(const uint16_t val) const;

  // Friends
  friend std::ostream& operator<<(std::ostream& os, Bits& b);

private:
  bool explanation_;
  uint16_t* bits_;
  uint16_t  num_data_bits_;
  uint16_t  num_check_bits_;
}; // Bits

#endif // _BITS_HPP_
