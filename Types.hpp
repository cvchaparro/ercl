/**
 * Types.hpp
 *
 * Typedefs to be used in the program.
 *
 * Cameron Vincent Chaparro
 */

#ifndef _TYPES_HPP_
#define _TYPES_HPP_

typedef unsigned char     uint8_t;
typedef unsigned short    uint16_t;
typedef unsigned int      uint32_t;
typedef unsigned long int uint64_t;

typedef signed char     int8_t;
typedef signed short    int16_t;
typedef signed int      int32_t;
typedef signed long int int64_t;

#include <string>

typedef std::string string;

#endif // _TYPES_HPP_
