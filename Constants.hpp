/**
 * file: Constants.hpp
 *
 * brief: This file contains some constants used by the program.
 *
 * author: Cameron Vincent Chaparro
 * date: 25 April 2014
 */

#ifndef _CONSTANTS_HPP_
#define _CONSTANTS_HPP_

// Custom headers
#include "Types.hpp"

// Holds the highest power of two that should be checked.
const uint16_t HIGHEST_POWER_OF_TWO = 5;

// Holds the length of the menu border.
const uint16_t BORDER_LENGTH = 58;

#endif // _CONSTANTS_HPP_
